﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectAndDisplay
{
    class Employee
    {
        private static int lastID =0;

        private int id = 0;
        private string givenName = "UNKNOWN";
        private string lastName = "UNKNOWN";
        private string department = "UNKNOWN";
        private string photo = "UNKNOWN";
        private float payRate = 0.0f;
        private float hoursWorked = 0.0f;

        public Employee()
        {
            this.ID = lastID+1;
        }

        /**************************************************/
        /* Getters and Setters                            */
        /**************************************************/
        public string GivenName
        {
            get { return givenName; }
            set { givenName = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public float PayRate
        {
            get { return payRate; }
            set { payRate = value; }
        }

        public string Photograph
        {
            get { return photo; }
            set { photo = value; }
        }

        public float HoursWorked
        {
            get { return hoursWorked; }
            set { hoursWorked = value; }
        }

        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        public int ID
        {
            get { return id; }
            set
            {
                if (value < lastID || id <= 0 || value <=0)
                {
                    id = lastID;
                }
                else
                {
                    id = value;
                }
                lastID = id+1;
            }
        }

        public string FullNameGL
        {
            get { return GivenName + " " + LastName; }
        }

        public string FullNameLG
        {
            get { return LastName + ", " + GivenName; }
        }

        public float Pay
        {
            get { return CalculatePay(); }
        }

        /*************************************************/
        /* Methods for the class                         */
        /*************************************************/

        private float CalculatePay()
        {
            float payment = 0.0f;
            payment = PayRate * HoursWorked;
            return payment;
        }

        public override string ToString()
        {
            return ID + ") " + FullNameLG + " (" + Department + ")";
        }
    }
}
