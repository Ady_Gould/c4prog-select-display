﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SelectAndDisplay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Employee> employeeList = new List<Employee>();
        List<Employee> payList = new List<Employee>();

        public MainWindow()
        {
            InitializeComponent();
            InitialiseEmployeeList();
            FillEmployeeListbox();
            ErrorLabel.Visibility = Visibility.Hidden;
        }

        private void FillEmployeeListbox()
        {
            //int count = employeeList.Count;
            //for (int i=0; i < count; i++)
            //{
            //    Employee emp = new Employee();
            //    emp = employeeList.ElementAt(i);
            //    EmployeeLB.Items.Add(emp.FullName);
            //}

            foreach (Employee emp in employeeList)
             {
                EmployeeLB.Items.Add(emp.FullNameLG);
                EmployeeListView.Items.Add(emp); // debugging listbox
            }

        }

        private void InitialiseEmployeeList()
        {
            Employee emp = new Employee();
            emp.ID = 1;
            emp.GivenName = "Fred";
            emp.LastName = "Flintstone";
            emp.PayRate = 34.56f;
            emp.Department = "Rock Crushing";
            emp.Photograph = "avatar-12";
            employeeList.Add(emp);

            emp = new Employee();
            emp.GivenName = "Betty";
            emp.LastName = "Rubble";
            emp.PayRate = 101.00f;
            emp.Department = "CTO";
            emp.Photograph = "avatar-27";
            employeeList.Add(emp);

            emp = new Employee(); // create new Employee object
            emp.ID = 5;
            emp.GivenName = "Barney";
            emp.LastName = "Rubble";
            emp.PayRate = 36.17f;
            emp.Department = "Rock Doctor";
            emp.Photograph = "avatar-10";
            employeeList.Add(emp);

            emp = new Employee();
            emp.GivenName = "Bam-Bam";
            emp.LastName = "Rubble";
            emp.PayRate = 84.00f;
            emp.Department = "Foreman";
            emp.Photograph = "avatar-2";
            employeeList.Add(emp);

            emp = new Employee();
            emp.GivenName = "Pebbles";
            emp.LastName = "Flintstone";
            emp.PayRate = 101.00f;
            emp.Department = "CEO";
            emp.Photograph = "avatar-24";
            employeeList.Add(emp);

            emp = new Employee();
            emp.GivenName = "Wilma";
            emp.LastName = "Flintstone";
            emp.PayRate = 81.40f;
            emp.Department = "CFO";
            emp.Photograph = "avatar-34";
            employeeList.Add(emp);

        }

        private void EmployeeLB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Employee theEmployee = new Employee();
            int selected = EmployeeLB.SelectedIndex;
            if (selected >= 0)
            {
                theEmployee = employeeList.ElementAt(selected);

                // Based on:
                // https://stackoverflow.com/questions/11880946/
                //              how-to-load-image-to-wpf-in-runtime
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri(
                    "pack://application:,,,/Resources/" +
                    theEmployee.Photograph + ".png");
                image.EndInit();
                EmployeePicture.Source = image;

                EmployeeName.Content = theEmployee.FullNameLG;
                HoursWorked.Focus();
                HoursWorked.SelectAll();
            }
            else
            {
                EmployeeName.Content = "";
                EmployeePicture.Source = new BitmapImage();
            }
        }

        private void HoursWorked_TextChanged(object sender, TextChangedEventArgs e)
        {
            // ignore me
        }

        private void PaymentButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            Employee theEmployee = new Employee();
            if (EmployeeLB.SelectedIndex >= 0)
            {
                int selected = EmployeeLB.SelectedIndex;
                theEmployee = employeeList[selected];
                try
                {
                    double hrsWorked = Convert.ToDouble(HoursWorked.Text);
                    theEmployee.HoursWorked = (float)hrsWorked;
                    if (hrsWorked < 0)
                    {
                        ErrorLabel.Content = "Hours worked must be zero or above";
                        ErrorLabel.Visibility = Visibility.Visible;
                        HoursWorked.SelectAll();
                        HoursWorked.Focus();
                    }
                    else
                    {
                        float paid = theEmployee.Pay;
                        payList.Add(theEmployee);
                        PayLB.Items.Add(theEmployee.FullNameLG + " " + paid);
                        EmployeeLB.SelectedIndex = -1;
                        HoursWorked.Text = "0";
                    }
                }
                catch (Exception ex)
                {
                    ErrorLabel.Content = "Hours Worked must be a positive number";
                    ErrorLabel.Visibility = Visibility.Visible;
                    HoursWorked.SelectAll();
                    HoursWorked.Focus();
                }
            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            PayLB.Items.Clear();
        }

        private void PaySlipsButton_Click(object sender, RoutedEventArgs e)
        {
            float totalPay = 0;
            foreach (Employee emp in payList)
            {
                // Process the employee
                // Create the payslip
                // Save and print payslip

                // add pay to the month total.
                totalPay += emp.Pay;
            }
            MessageBox.Show("Total payments: " + totalPay);

        }
    }
}
